from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='fallaughter-bot',
    version='0.0.0',
    description='Telegram scraper bot',
    license='Apache License 2.0',
    author='Zavmerov Nikita',
    author_email='McClawdy@gmail.com',
    packages=['fallaughter_bot'],
    entry_points={
        'console_scripts': [
            'bot_sart = fallaughter_bot.fallaughter_bot:main',
        ],
    },
    install_requires = requirements,
    package_data={
        'fallaughter_bot': ['resources/*'],
    }
)