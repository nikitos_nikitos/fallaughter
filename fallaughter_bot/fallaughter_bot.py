import scrapy
import telebot
import time
import json 
import inspect
import sys
import os

from progress.bar import IncrementalBar
from scrapy.crawler import CrawlerProcess
from spiders import reddit_img_spider



CONFIG_PATH = '../resources/config.json'

with open(CONFIG_PATH) as f:
    cfg = json.loads(f.read())
    
bot = telebot.TeleBot(cfg.get('bot').get('api'))
users = cfg.get('bot').get('users')

class FalLaughterBot(object):
    def __init__(self):

        self.__sources = cfg.get('bot').get('sources')
        self.__types = cfg.get('bot').get('types')

        self._sources_keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self._boards_keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self._types_keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self.parse = telebot.types.ReplyKeyboardMarkup(True)

        self._sources_keyboard.row(*list(self.__sources.keys()))
        self._types_keyboard.row(*self.__types)
        self.parse.row('Да', 'Нет')
        
        self.options = {}

    def get_sources(self):
        return self.__sources

    def get_types(self):
        return self.__types

    def get_sources_keyboard(self):
        return self._sources_keyboard

    def get_boards_keyboard(self, msg):
        _boards_keyboard = telebot.types.ReplyKeyboardMarkup(True)
        if msg in list(self.__sources.keys()):
            _boards_keyboard.row(*self.__sources[msg])
            return _boards_keyboard

    def get_types_keyboard(self):
        return  self._types_keyboard

    def get_class_name(self):
        classname = self.options.get("source").capitalize() + self.options.get("board").capitalize() + self.options.get("type").capitalize() + 'Spider'
        return classname


fallaughter = FalLaughterBot()


# class RedditFunnyImgSpider(scrapy.Spider):
#     name = 'reddit_funny_img'
#     allowed_domains = ['www.reddit.com']
#     start_urls = [f'https://www.reddit.com/r/{fallaughter.options.get("board")}/']

#     custom_settings = {
#         'DEPTH_LIMIT': 10
#     }

#     def parse(self, response):
#         titles = response.xpath('//*[@class="_eYtD2XCVieq6emjKBH3m"]/text()').extract()
#         imgs = response.xpath('//img[@alt="Post image"]/@src').extract()
       
#         for (title, img) in zip(titles, imgs):
#             with open('reddit_img.json', 'a') as f:
#               yield  json.dump({'Title': title, 'Image': img} , f)
        
#         next_page = response.xpath('//link[@rel="next"]/@href').extract_first()
#         if next_page is not None:
#             yield response.follow(next_page, self.parse)


@bot.message_handler(func=lambda message: message.chat.id not in users)
def checker(message):
    bot.send_message(message.chat.id, "Вы не можете использовать данного бота")


@bot.message_handler(commands = ['start'])
def say_hello(message):
    bot.send_message(message.chat.id, "Привет, начнем")
    time.sleep(0.3)
    source = bot.send_message(message.chat.id, "Выберите ресурс", reply_markup=fallaughter.get_sources_keyboard())
    time.sleep(0.3)
    bot.register_next_step_handler(source, choose_board)
  

@bot.message_handler(commands = fallaughter.get_sources().keys())
def choose_board(message):
    fallaughter.options['source'] = message.text
    board = bot.send_message(message.chat.id, "Выберите доску", reply_markup=fallaughter.get_boards_keyboard(message.text))
    time.sleep(0.3)
    bot.register_next_step_handler(board, choose_type)
    

@bot.message_handler(commands = fallaughter.get_types())
def choose_type(message):
    fallaughter.options['board'] = message.text
    _type = bot.send_message(message.chat.id, "Выберите тип данных", reply_markup=fallaughter.get_types_keyboard())
    time.sleep(0.3)


@bot.message_handler(content_types = ['text'])    
def show_info(message):
    fallaughter.options['type'] = message.text
    source = fallaughter.options.get('source')
    board = fallaughter.options.get('board')
    _type = fallaughter.options.get('type')
    bot.send_message(message.chat.id, f"Выбран {source}, доска - {board}, тип данных - {_type}")
    parse = bot.send_message(message.chat.id, 'Начать парсинг?', reply_markup = fallaughter.parse)
    bot.register_next_step_handler(parse, req_parse)
    
    
@bot.message_handler(content_types = ['text'])
def req_parse(message):
    if message.text.lower() == 'да':
        bot.register_next_step_handler(message, start_parse)
    else:
        say_hello(message)

@bot.message_handler(commands = ['parse'])
def start_parse(message):
    name = f'{fallaughter.options.get("source")}_{fallaughter.options.get("type")}'
    os.system(f'python spiders/{name}_spider.py')
    with open(f'{name}.json', 'r') as f:
        for i in f:
            print(f'i {i.strip("[]")}')
            print(type(i))
            
        
        

    
def main():
    bot.polling()
    
if __name__ == '__main__':
    main()