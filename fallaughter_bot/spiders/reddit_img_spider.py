import scrapy
import json
from scrapy.crawler import CrawlerProcess
import os

class RedditImgSpider(scrapy.Spider):
    name = 'reddit_funny_img'
    allowed_domains = ['www.reddit.com']
    start_urls = ['https://www.reddit.com/r/funny/']

    custom_settings = {
        'DEPTH_LIMIT': 10
    }

    def parse(self, response):
        titles = response.xpath('//*[@class="_eYtD2XCVieq6emjKBH3m"]/text()').extract()
        imgs = response.xpath('//img[@alt="Post image"]/@src').extract()

        with open('reddit_img.json', 'a') as f:
            for img in zip(imgs):
                yield  json.dump(img, f)

        
           
        
        next_page = response.xpath('//link[@rel="next"]/@href').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)



process = CrawlerProcess()
process.crawl(RedditImgSpider)
process.start() 